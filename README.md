# README #


# Testing #
### ¿Qué es testing?
Son las pruebas que se hacen a los programas o productos de software durante su desarrollo o al finalizar el mismo, generalmente las hace el equipo técnico

### ¿Para qué se hace testing?
Para probar funcionalidades desarrolladas, las reglas del negocio, las funciones técnicas específicas. 

### ¿Cómo hacer testing?
Probando en ambiente de desarrollo, en ambiente local, generalmente ingresando a la misma aplicación y haciendo pruebas de las funcionalidades o funciones ténicas de la aplicación. 

### ¿Herramientas de testing?
Navegadores web, postman, soapui, ide's de desarrollo. 



# QA #
### ¿Qué es QA?
Son pruebas que se hacen a los programas o productos informáticos, generalmente realizadas por personas mas funcionales con ciertos conocimientos técnicos, que conocen bien el requerimiento de negocio

### ¿Para qué se hace QA?
Su fin es garantizar que el software no tiene errores ni ténicos ni funcionales,  y que cumple con los requerimientos del negocio.

### ¿Cómo hacer QA?
Se prepara un ambiente bastante cercano al ambiente de producción, se despliega el software y los analista de QA realizan sus pruebas, generalmente con set de pruebas ya escritos, los cuales muchas veces contiene hasta los resultados esperados. 

### ¿Herramientas de QA?
Navegadores web, la aplicación misma, nexus


# QA Automation #
### ¿Qué es  QA Automation?
Es la ejecución de pruebas de QA pero de forma automática, pudiera ser dentro de un pipeline. 

### ¿Para qué se hace  QA Automation?
Para garantizar  no tiene errores ni ténicos ni funcionales,  y que cumple con los requerimientos del negocio.

### ¿Cómo hacer  QA Automation?
programando un set de pruebas y agregandolas al pipeline para que se ejecuten de manera automática. 

### ¿Herramientas de  QA Automation?
Jenkins, Nexus, Selenium





--------------------------------------------------------------------------------------------





This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact